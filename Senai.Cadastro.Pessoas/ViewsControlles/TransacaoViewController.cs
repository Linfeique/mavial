using System;
using System.Collections.Generic;
using System.IO;
using Senai.Cadastro.Pessoas.Models;
using Senai.Cadastro.Pessoas.ViewsControlles;

namespace Senai.Cadastro.Pessoas.ViewsControlles {
    public class TransacaoViewController {
        public int resposta_menu;

        public void MenuFinanceiroAdmin () {

            RelatorioUsuario usuarios = new RelatorioUsuario ();
            RelatorioTransacoes transacoes = new RelatorioTransacoes ();
            Compacta compactar = new Compacta ();

            Console.Clear ();
            System.Console.WriteLine ("--------- Bem-Vindo ao Menu Financeiro Admin---------");
            System.Console.WriteLine ("1- Cadastrar Transação");
            System.Console.WriteLine ("2- Extrato de transações total");
            System.Console.WriteLine ("3- Gerar Relatório de Todos os usuários");
            System.Console.WriteLine ("4- Gerar Relatório de Transações Pessoais");
            System.Console.WriteLine ("5- Compactar banco de dados");
            System.Console.WriteLine ("0- Sair");

            System.Console.WriteLine ("\nEscolha uma opção: ");
            resposta_menu = int.Parse (Console.ReadLine ());

            switch (resposta_menu) {
                case 1:
                    CadastroTransacao ();
                    break;

                case 2:
                    ExtratoTotal ();
                    break;

                case 3:
                    usuarios.GerarRelatorioUsuarios ();
                    break;
                case 4:
                    transacoes.GerarRelatorioTransacoes ();
                    break;

                case 5:
                    compactar.CompactarAdmin();
                    break;

                case 0:

                    break;

                default:
                    System.Console.WriteLine ("Opção Inválida");
                    break;
            }
        }

        public void MenuFinanceiroComum () {

            RelatorioUsuario usuarios = new RelatorioUsuario ();
            RelatorioTransacoes transacoes = new RelatorioTransacoes ();
            Compacta compactar = new Compacta ();

            Console.Clear ();
            System.Console.WriteLine ("--------- Bem-Vindo ao Menu Financeiro ---------");
            System.Console.WriteLine ("1- Cadastrar Transação");
            System.Console.WriteLine ("2- Extrato de transações total");
            System.Console.WriteLine ("3- Gerar Relatório de Transações Pessoais");
            System.Console.WriteLine ("4- Compactar banco de dados");
            System.Console.WriteLine ("0- Sair");

            System.Console.WriteLine ("\nEscolha uma opção: ");
            resposta_menu = int.Parse (Console.ReadLine ());

            switch (resposta_menu) {
                case 1:
                    CadastroTransacao ();
                    break;

                case 2:
                    ExtratoTotal ();
                    break;

                case 3:
                    transacoes.GerarRelatorioTransacoes ();
                    break;

                case 4:
                    compactar.CompactarComum();
                    break;

                case 0:

                    break;

                default:
                    System.Console.WriteLine ("Opção Inválida");
                    break;
            }
        }

        public void CadastroTransacao () {
            TransacaoViewModel transacaoViewModel = new TransacaoViewModel ();
            bool resposta_transacao_achou = false;
            int resposta_transacao = 0;

            do {
                System.Console.WriteLine ("Qual o tipo da transação [0 ou 1]: ");
                System.Console.WriteLine ("Obs: 0 é Despesa e 1 é Receita");
                resposta_transacao = int.Parse (Console.ReadLine ());
                if (resposta_transacao < 0 || resposta_transacao > 1) {
                    System.Console.WriteLine ("Por favor digite 0 ou 1");
                } else {
                    transacaoViewModel.TipoTransacao = resposta_transacao;
                    resposta_transacao_achou = true;
                    break;
                }
            } while (!resposta_transacao_achou);

            System.Console.WriteLine ("Digite uma descrição: ");
            transacaoViewModel.Descricao = Console.ReadLine ();

            System.Console.WriteLine ("A transação é na data atual [sim/não]");
            string resposta_data = Console.ReadLine ();

            switch (resposta_data) {
                case "sim":
                    transacaoViewModel.DataTransacao = DateTime.Now;
                    break;

                case "nao":
                case "não":
                    System.Console.WriteLine ("Informe a data da transação [DD/MM/AAAA]");
                    transacaoViewModel.DataTransacao = DateTime.Parse (Console.ReadLine ());
                    break;

                default:
                    Console.WriteLine ("Opção Inválida");
                    break;
            }

            System.Console.WriteLine ("Digite o valor da transação: ");
            transacaoViewModel.Valor = float.Parse (Console.ReadLine ());
            if (transacaoViewModel.Valor < 0) {
                System.Console.WriteLine ("Dados sendo escritos no arquivo");
                System.Console.WriteLine ("Aperte alguma tecla");
                Console.ReadKey ();
                Console.Clear ();
            } else if (resposta_transacao == 0) {
                transacaoViewModel.Valor = transacaoViewModel.Valor - (transacaoViewModel.Valor * 2);
            }

            Program programa = new Program();

            using(StreamWriter escrever = new StreamWriter(@"..\Bancos_de_dados\transacoes.csv", true)){
                escrever.WriteLine($"{transacaoViewModel.TipoTransacao};{transacaoViewModel.Descricao};{transacaoViewModel.DataTransacao};{transacaoViewModel.Valor}");
            }
            MenuFinanceiroAdmin ();
        }

        public void ExtratoTotal () {
            TransacaoViewModel transacaoViewModel = new TransacaoViewModel ();
            List<TransacaoViewModel> tsvalores = new List<TransacaoViewModel> ();
            float valorTotal = 0;
            TransacaoViewModel valores = new TransacaoViewModel();;
            string[] linhas = File.ReadAllLines(@"..\Bancos_de_dados\transacoes.csv");

            
            foreach (string item in linhas){
                string[] dados = item.Split(";");
                valores.Valor = float.Parse(dados[3]);
                tsvalores.Add(valores);
                valorTotal = valorTotal + valores.Valor;
            }

            Console.WriteLine ($"O total de extratos é: {valorTotal.ToString("C")}");
            Console.WriteLine ("Aperte alguma tecla");
            Console.ReadKey ();
            MenuFinanceiroAdmin ();
        }

        public List<TransacaoViewModel> ListarTransacoes () {
            #region Controller
                List<TransacaoViewModel> lstransacoes = new List<TransacaoViewModel>();
                TransacaoViewModel transacao;

                string[] linhas = File.ReadAllLines(@"..\Bancos_de_dados\transacoes.csv");

                foreach (string item in linhas){
                    string[] dados = item.Split(";");
                    transacao = new TransacaoViewModel();
                    transacao.TipoTransacao = int.Parse(dados[0]);
                    transacao.Descricao = dados[1];
                    transacao.DataTransacao = DateTime.Parse(dados[2]);
                    transacao.Valor = float.Parse(dados[3]);
                    lstransacoes.Add(transacao);
                }

            return lstransacoes;
            #endregion
        }
    }
}