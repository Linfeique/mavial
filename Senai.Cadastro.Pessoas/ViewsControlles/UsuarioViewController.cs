using System;
using System.Collections.Generic;
using System.IO;
using Senai.Cadastro.Pessoas.Models;
using Senai.Cadastro.Pessoas;

namespace Senai.Cadastro.Pessoas.ViewsControlles
{
    public class UsuarioViewController
    {
        int contador = 1;

        public void Menu(){
            System.Console.WriteLine("--------- Bem-Vindo ---------");
            System.Console.WriteLine("1 - Cadastrar");
            System.Console.WriteLine("2 - Logar");
            System.Console.WriteLine("0 - Sair");
            System.Console.WriteLine("-----------------------------");

            System.Console.WriteLine("\nEscolha uma opção: ");
        }

        public bool Verificacao_Email(string email){
            if(email.Contains("@") && email.Contains(".")){
                return true;
            } else{
                return false;
            }
        }

        public bool Verificacao_Senha(string senha){
            if(senha.Length >= 6){
                return true;
            } else{
                return false;
            }
        }

        public bool Verificacao_Data(string data){
            if(DateTime.TryParse(dataNascimento, out data_certa)){
                    
            }
        }

        public void Cadastrar(){
            string email;
            string senha;
            string dataNascimento;
            DateTime data_certa;
            bool senha_certa = false;
            bool email_certo = false;

            #region View
                UsuarioViewModel usuarioViewModel = new UsuarioViewModel();

                System.Console.WriteLine("Informe o seu nome: ");
                string nome = Console.ReadLine();
                
                do{
                    System.Console.WriteLine("Informe o seu email: ");
                    email = Console.ReadLine();

                    if(Verificacao_Email(email) == true){
                        email_certo = true;
                        break;
                    } else{
                        System.Console.WriteLine("Email inválido");
                    }
                } while(!email_certo);
                
                do{
                    System.Console.WriteLine("Informe sua senha: ");
                    senha = Console.ReadLine();

                    if(Verificacao_Senha(senha) == true){
                        senha_certa = true;
                        break;
                    } else{
                        System.Console.WriteLine("Senha Inválida");
                    }
                } while(!senha_certa);

                System.Console.WriteLine("Informe sua data de nascimento (DD/MM/AAAA)");
                dataNascimento = Console.ReadLine();

            #endregion

            #region Controller

                List<UsuarioViewModel> lsUsuarios = ListarUsuarios();

                usuarioViewModel.Id = lsUsuarios.Count + 1;
                usuarioViewModel.Nome = nome;
                usuarioViewModel.Email = email;
                usuarioViewModel.Senha = senha;
                usuarioViewModel.DataNascimento = DateTime.Parse(dataNascimento);

                /// <summary>
                /// 
                /// </summary>
                /// <returns></returns>

                using(StreamWriter escrever = new StreamWriter(@"..\Bancos_de_dados\usuarios.csv", true)){
                        escrever.WriteLine($"{usuarioViewModel.Id};{usuarioViewModel.Nome};{usuarioViewModel.Email};{usuarioViewModel.Senha};{usuarioViewModel.DataNascimento}");
                }

                System.Console.WriteLine("Usuário Cadastrado");
                contador++;
            #endregion
            Menu();
        }
    
        public string Logar(string email_login){
            bool senha_certa = false;
            
            #region View
                string senha_login;

                do{
                    System.Console.WriteLine("Informe sua senha: ");
                    senha_login = Console.ReadLine();

                    if(senha_login == "admin"){
                        senha_certa = true;
                        break;
                    }

                    if(Verificacao_Senha(senha_login) == true){
                        senha_certa = true;
                        break;
                    } else{
                        System.Console.WriteLine("Senha Inválida! Informe uma senha com mais de 6 dígitos.");
                    }
                } while(!senha_certa);

                
            #endregion

            #region Controller
                List<UsuarioViewModel> lsUsuarios = ListarUsuarios();
                TransacaoViewController transacaoView = new TransacaoViewController();

                foreach (UsuarioViewModel item in lsUsuarios)
                {

                    if(email_login == "admin" && senha_login == "admin"){
                        System.Console.WriteLine("Acesso liberado");
                        System.Console.WriteLine("Aperte alguma tecla");
                        Console.ReadKey();
                        Console.Clear();
                        transacaoView.MenuFinanceiroAdmin();
                        break;
                    }

                    if(email_login == item.Email && senha_login == item.Senha){
                        System.Console.WriteLine("Acesso liberado");
                        System.Console.WriteLine("Aperte alguma tecla");
                        Console.ReadKey();
                        Console.Clear();
                        transacaoView.MenuFinanceiroComum();
                        break;
                    }
                }
            #endregion
            return email_login;
        }
    
        public List<UsuarioViewModel> ListarUsuarios(){
            #region Controller
                List<UsuarioViewModel> lsUsuarios = new List<UsuarioViewModel>();
                UsuarioViewModel usuario;

                string[] linhas = File.ReadAllLines(@"..\Bancos_de_dados\usuarios.csv");

                foreach (string item in linhas){
                    string[] dados = item.Split(";");
                    usuario = new UsuarioViewModel();
                    usuario.Id = int.Parse(dados[0]);
                    usuario.Nome = dados[1];
                    usuario.Email = dados[2];
                    usuario.Senha = dados[3];
                    usuario.DataNascimento = DateTime.Parse(dados[4]);
                    lsUsuarios.Add(usuario);
                }

                return lsUsuarios;
            #endregion
        }

        public void AchaUsuario(){
            using(StreamReader ler = new StreamReader(@"..\Bancos_de_dados\usuarios.csv")){
                string leitura = ler.ReadToEnd();

            }
        }
    
    }
}