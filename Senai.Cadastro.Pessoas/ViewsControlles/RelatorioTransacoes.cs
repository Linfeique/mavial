using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using Senai.Cadastro.Pessoas.Models;
using Spire.Doc;
using Spire.Doc.Documents;
using Spire.Doc.Fields;

namespace Senai.Cadastro.Pessoas.ViewsControlles {
    public class RelatorioTransacoes {
        public void GerarRelatorioTransacoes () {
            UsuarioViewController usuarioDado = new UsuarioViewController();

            usuarioDado.ListarUsuarios();

            Document doc = new Document ();

            Section section = doc.AddSection ();
            #region nome
            Paragraph texto = section.AddParagraph ();

            texto.AppendText ($"\nNome:");
            texto.AppendText ($"\nE-mail:");
            texto.AppendText ($"\nData de nascimento:");
            
            ParagraphStyle s2 = new ParagraphStyle (doc);

            s2.Name = "BodyTexxtColor";
            s2.CharacterFormat.TextColor = Color.Black;

            //fonte
            s2.CharacterFormat.FontName = "Calibri";

            //tamanho
            s2.CharacterFormat.FontSize = 15;

            //alinhando justificado
            texto.Format.HorizontalAlignment = HorizontalAlignment.Justify;

            //espaço entre linhas
            texto.Format.BeforeSpacing = 3;

            //adicionando estilo
            doc.Styles.Add (s2);
            texto.ApplyStyle (s2.Name);
            #endregion
            #region Titulo
            Paragraph titulo = section.AddParagraph ();

            titulo.AppendText ("Relatório de transações");

            ParagraphStyle s1 = new ParagraphStyle (doc);

            s1.Name = "TitleTextColor";
            s1.CharacterFormat.TextColor = Color.DarkBlue;
            s1.CharacterFormat.FontName = "Microsoft JhengHei UI Light";
            s1.CharacterFormat.FontSize = 30;
            titulo.Format.HorizontalAlignment = HorizontalAlignment.Center;
            titulo.Format.BeforeSpacing = 30;
            titulo.Format.AfterSpacing = 30;

            doc.Styles.Add (s1);
            titulo.ApplyStyle (s1.Name);
            #endregion

            #region tabela
            Table tabelaTransacao = section.AddTable (true);

            String[] Header = { "Data", "Tipo", "Descrição", "Valor" };

            TransacaoViewController transacaoRepositorio = new TransacaoViewController ();

            List<TransacaoViewModel> lstransacao = transacaoRepositorio.ListarTransacoes ();

            String[][] dadosTransacao = new string[lstransacao.Count][];

            for (int contListaTransacao = 0; contListaTransacao < lstransacao.Count; contListaTransacao++) {
                dadosTransacao[contListaTransacao] = lstransacao[contListaTransacao].ConverterT ();
            }

            tabelaTransacao.ResetCells (dadosTransacao.Length + 1, Header.Length);

            //criando a linha do cabeçalho

            TableRow headerRow = tabelaTransacao.Rows[0];

            headerRow.IsHeader = true;

            //definindo a altura da linha

            headerRow.Height = 20;

            //definindo formato do cabeçalho

            headerRow.RowFormat.BackColor = Color.LightGray;

            for (int contTransacao1 = 0; contTransacao1 < Header.Length; contTransacao1++) {

                //alinhamento da celula

                Paragraph pTransacao1 = headerRow.Cells[contTransacao1].AddParagraph ();

                headerRow.Cells[contTransacao1].CellFormat.VerticalAlignment = VerticalAlignment.Middle;

                pTransacao1.Format.HorizontalAlignment = HorizontalAlignment.Center;

                //formatação do cabeçalho

                TextRange tabelaDadosTransacao = pTransacao1.AppendText (Header[contTransacao1]);

                tabelaDadosTransacao.CharacterFormat.FontName = "Times New Roman";

                tabelaDadosTransacao.CharacterFormat.FontSize = 12;

                tabelaDadosTransacao.CharacterFormat.TextColor = Color.Black;

                tabelaDadosTransacao.CharacterFormat.Bold = true;

            }

            // criando linhas da parte de dados

            for (int contTransacao2 = 0; contTransacao2 < dadosTransacao.Length; contTransacao2++) {

                TableRow dataTransacaoRow = tabelaTransacao.Rows[contTransacao2 + 1];

                //altura da linha

                dataTransacaoRow.Height = 17;

                //representando as colunas

                for (int contTransacao3 = 0; contTransacao3 < dadosTransacao[contTransacao2].Length; contTransacao3++) {

                    //alinhamento das celulas

                    dataTransacaoRow.Cells[contTransacao3].CellFormat.VerticalAlignment = VerticalAlignment.Middle;

                    //preenchendo as linhas com os dados

                    Paragraph pDadosTransacao = dataTransacaoRow.Cells[contTransacao3].AddParagraph ();

                    TextRange tabelaPreenchidaTransacao = pDadosTransacao.AppendText (dadosTransacao[contTransacao2][contTransacao3]);

                    //formantando as celulas

                    pDadosTransacao.Format.HorizontalAlignment = HorizontalAlignment.Center;

                    tabelaPreenchidaTransacao.CharacterFormat.FontName = "Times New Roman";

                    tabelaPreenchidaTransacao.CharacterFormat.FontSize = 10;

                    tabelaPreenchidaTransacao.CharacterFormat.TextColor = Color.Black;

                }

            }

            #endregion

            #region marca d'agua

            // PictureWatermark picture = new PictureWatermark ();
            // picture.Picture = System.Drawing.Image.FromFile (@"C:\Users\52232950808\Documents\SENAI\SPRINT 4\mobtec_logosite.png");
            // picture.Scaling = 20;
            // picture.IsWashout = false;
            // doc.Watermark = picture;

            Console.WriteLine ("Informe o nome com qual deseja salvar o arquivo: ");

            string nomeArquivoUsuario = Console.ReadLine ();

            //salvando o arquivo e gerando documento

            doc.SaveToFile ($"C://Users//{Environment.UserName}//Desktop//{nomeArquivoUsuario}.docx");

            //avisando o usuario que o arquivo foi gerado

            Console.WriteLine ("Relatório Gerado com Sucesso! Seu arquivo já está disponível no desktop.");

            #endregion
        }
    }
}