using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using Senai.Cadastro.Pessoas.Models;
using Spire.Doc;
using Spire.Doc.Documents;
using Spire.Doc.Fields;
namespace Senai.Cadastro.Pessoas.ViewsControlles {
    public class RelatorioUsuario {

        public void GerarRelatorioUsuarios () {

            Document doc = new Document ();

            Section section = doc.AddSection ();
            #region Titulo
            Paragraph titulo = section.AddParagraph ();

            titulo.AppendText ("Relatório de Usuários");

            ParagraphStyle s1 = new ParagraphStyle (doc);

            s1.Name = "TitleTextColor";
            s1.CharacterFormat.TextColor = Color.DarkBlue;
            s1.CharacterFormat.FontName = "Microsoft JhengHei UI Light";
            s1.CharacterFormat.FontSize = 30;
            titulo.Format.HorizontalAlignment = HorizontalAlignment.Center;
            titulo.Format.BeforeSpacing = 30;
            titulo.Format.AfterSpacing = 30;

            doc.Styles.Add (s1);
            titulo.ApplyStyle (s1.Name);
            #endregion
            #region Tabela
            Table tabelaUsuario = section.AddTable (true);

            //criando o cabeçalho e adicionando os dados

            String[] headerUsuario = { "ID", "NOME", "EMAIL", "DATA DE NASCIMENTO" };

            UsuarioViewController usuarioRepositorio = new UsuarioViewController ();

            List<UsuarioViewModel> lsusuarios = usuarioRepositorio.ListarUsuarios ();

            String[][] dadosListaUsuario = new string[lsusuarios.Count][];

            for (int contListaUsuario = 0; contListaUsuario < lsusuarios.Count; contListaUsuario++) {
                dadosListaUsuario[contListaUsuario] = lsusuarios[contListaUsuario].Converter ();
            }

            //resetando a tabela e criando as celulas

            tabelaUsuario.ResetCells (dadosListaUsuario.Length + 1, headerUsuario.Length);

            //criando a linha do cabeçalho

            TableRow headerRow = tabelaUsuario.Rows[0];

            headerRow.IsHeader = true;

            //definindo a altura da linha

            headerRow.Height = 20;

            //definindo formato do cabeçalho

            headerRow.RowFormat.BackColor = Color.LightGray;

            for (int contUsuario1 = 0; contUsuario1 < headerUsuario.Length; contUsuario1++) {

                //alinhamento da celula

                Paragraph pUsuario1 = headerRow.Cells[contUsuario1].AddParagraph ();

                headerRow.Cells[contUsuario1].CellFormat.VerticalAlignment = VerticalAlignment.Middle;

                pUsuario1.Format.HorizontalAlignment = HorizontalAlignment.Center;

                //formatação do cabeçalho

                TextRange tabelaDadosUsuario = pUsuario1.AppendText (headerUsuario[contUsuario1]);

                tabelaDadosUsuario.CharacterFormat.FontName = "Times New Roman";

                tabelaDadosUsuario.CharacterFormat.FontSize = 12;

                tabelaDadosUsuario.CharacterFormat.TextColor = Color.Black;

                tabelaDadosUsuario.CharacterFormat.Bold = true;

            }

            // criando linhas da parte de dados

            for (int contUsuario2 = 0; contUsuario2 < dadosListaUsuario.Length; contUsuario2++) {

                TableRow dataUsuarioRow = tabelaUsuario.Rows[contUsuario2 + 1];

                //altura da linha

                dataUsuarioRow.Height = 17;

                //representando as colunas

                for (int contUsuario3 = 0; contUsuario3 < 4; contUsuario3++) {

                    //alinhamento das celulas

                    dataUsuarioRow.Cells[contUsuario3].CellFormat.VerticalAlignment = VerticalAlignment.Middle;

                    //preenchendo as linhas com os dados

                    Paragraph pDadosUsuario = dataUsuarioRow.Cells[contUsuario3].AddParagraph ();

                    TextRange tabelaPreenchidaUsuario = pDadosUsuario.AppendText (dadosListaUsuario[contUsuario2][contUsuario3]);

                    //formantando as celulas

                    pDadosUsuario.Format.HorizontalAlignment = HorizontalAlignment.Center;

                    tabelaPreenchidaUsuario.CharacterFormat.FontName = "Arial";

                    tabelaPreenchidaUsuario.CharacterFormat.FontSize = 10;

                    tabelaPreenchidaUsuario.CharacterFormat.TextColor = Color.Black;

                }

            }

            #endregion
            // PictureWatermark picture = new PictureWatermark ();
            // picture.Picture = System.Drawing.Image.FromFile (@"C:\Users\52232950808\Documents\SENAI\SPRINT 4\mobtec_logosite.png");
            // picture.Scaling = 20;
            // picture.IsWashout = false;
            // doc.Watermark = picture;

            Console.WriteLine ("Informe o nome com qual deseja salvar o arquivo: ");

            string nomeArquivoUsuario = Console.ReadLine ();

            //salvando o arquivo e gerando documento

            doc.SaveToFile ($"C://Users//{Environment.UserName}//Desktop//{nomeArquivoUsuario}.docx");

            //avisando o usuario que o arquivo foi gerado

            Console.WriteLine ("Relatório Gerado com Sucesso! Seu arquivo já está disponível no desktop.");

        }

    }

}