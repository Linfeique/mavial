using System;
using System.IO;
using System.IO.Compression;

namespace Senai.Cadastro.Pessoas.ViewsControlles
{
    public class Compacta
    {
        public void CompactarComum(){

            TransacaoViewController transacao = new TransacaoViewController();

            if(!Directory.Exists(@"..\Bancos_de_dados")){
                System.Console.WriteLine("Diretório não encontrado");
            } else{
                ZipFile.CreateFromDirectory(@"C:\Users\44846245837\Desktop\mavial\Bancos_de_dados", "banco_de_dados.zip");
            }
            transacao.MenuFinanceiroComum();
        }

        public void CompactarAdmin(){
            TransacaoViewController transacao = new TransacaoViewController();

            if(!Directory.Exists(@"..\Bancos_de_dados")){
                System.Console.WriteLine("Diretório não encontrado");
            } else{
                ZipFile.CreateFromDirectory(@"C:\Users\44846245837\Desktop\mavial\Bancos_de_dados", "banco_de_dados.zip");
            }
            transacao.MenuFinanceiroAdmin();
        }
    }
}