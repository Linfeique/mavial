﻿using System;
using System.IO;
using Senai.Cadastro.Pessoas.ViewsControlles;

namespace Senai.Cadastro.Pessoas
{
    class Program
    {

        public static string pegaUsuario;

        static void Main(string[] args)
        {
            UsuarioViewController usuarioView = new UsuarioViewController();

            usuarioView.Menu();
            int resposta = int.Parse(Console.ReadLine());
            System.Console.WriteLine("");
            string email_login;
            bool email_certo = false;
            
            switch (resposta)
            {
                case 1:
                    usuarioView.Cadastrar();
                break;
                
                case 2:
                    do{
                        System.Console.WriteLine("Informe o seu email: ");
                        email_login = Console.ReadLine();

                        if(email_login == "admin"){
                            email_certo = true;
                            break;
                        }

                        if(usuarioView.Verificacao_Email(email_login) == true){
                            email_certo = true;
                            break;
                        } else{
                            System.Console.WriteLine("Email inválido! Informe um e-mail com '@' e '.' ");
                        }
                    } while(!email_certo);
                    pegaUsuario = usuarioView.Logar(email_login);
                break;

                default:
                    System.Console.WriteLine("Opção inválida");
                break;
            }
        }
    }
}
