using System;

namespace Senai.Cadastro.Pessoas.Models {
    public class TransacaoViewModel {
        public int TipoTransacao { get; set; }
        public string Descricao { get; set; }
        public DateTime DataTransacao { get; set; }
        public float Valor { get; set; }
        public string[] ConverterT () => new string[] { TipoTransacao.ToString (), Descricao, DataTransacao.ToString (), Valor.ToString () };

    }
}